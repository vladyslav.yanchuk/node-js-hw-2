const request = async (e, url, method, form) => {
  e.preventDefault();

  const data = {};
  const inputs = form.elements;

  for (i = 0; i < inputs.length; i++) {
    if (inputs[i].nodeName === 'INPUT' && inputs[i].type === 'text') {
      data[inputs[i].name] = inputs[i].value;
    }
  }

  try {
    const response = await fetch(url, {
      method,
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': jwt,
      },
    });
    const json = await response.json();

    if ( json.jwt_token ) {
      localStorage.setItem('token', `JWT ${json.jwt_token}`);
    }

    console.log(JSON.stringify(json));
  } catch (error) {
    console.error('Error:', error);
  }
};

const form = document.getElementById('form');
const jwt = localStorage.getItem('token');

form.onsubmit = (e) => {
  request(e, form.dataset.url, form.dataset.method, form, jwt);
};
