const noteInput = document.getElementById('note');
const createBtn = document.getElementById('create');
const notesList = document.getElementById('list');

const request = async (url, method, data, jwt) => {
  try {
    const options = {
      method,
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': jwt,
      },
    };

    const response = await fetch(url, options);
    const json = await response.json();
    console.log(json);
    return json;
  } catch (error) {
    console.error('Ошибка:', error);
  }
};

createBtn.onclick = (e) => {
  const jwt = localStorage.getItem('token');
  request('http://localhost:8080/api/notes', 'POST', {text: noteInput.value}, jwt);
  noteInput.value = '';
  refresh();
};

const refresh = async () => {
  notesList.innerHTML = '';
  const jwt = localStorage.getItem('token');
  const data = await request('http://localhost:8080/api/notes', 'GET', undefined, jwt);
  console.log(data.notes);
  if ( !data.notes.length ) {
    const noNotes = document.createElement('span');
    noNotes.innerText = 'No notes found!';
    notesList.appendChild(noNotes);
  }

  data.notes.forEach((el) => {
    const note = document.createElement('li');
    const text = document.createElement('span');
    const deleteBtn = document.createElement('button');
    const editBtn = document.createElement('button');

    deleteBtn.classList.add('delete-btn');
    editBtn.classList.add('edit-btn');
    text.classList.add('note-text');

    deleteBtn.innerText = 'x';
    editBtn.innerText = 'edit';

    el.completed && text.classList.add('completed');

    text.innerText = el.text;
    note.dataset.id = el._id;

    note.appendChild(text);
    note.appendChild(editBtn);
    note.appendChild(deleteBtn);
    notesList.appendChild(note);

    deleteBtn.onclick = (e) => {
      note.remove();
      request(`http://localhost:8080/api/notes/${el._id}`, 'delete', undefined, jwt);
      refresh();
    };

    editBtn.onclick = (e) => {
      const newNoteText = prompt('Enter new value:', text.innerText);
      text.innerText = newNoteText || text.innerText;
      newNoteText && request(`http://localhost:8080/api/notes/${el._id}`, 'put', {text: newNoteText}, jwt);
    };

    text.onclick = (e) => {
      request(`http://localhost:8080/api/notes/${el._id}`, 'PATCH', {}, jwt);
            text.classList.contains('completed') ?
            text.classList.remove('completed') :
            text.classList.add('completed');
    };
  });
};

window.addEventListener('load', (e) => {
  refresh();
});
