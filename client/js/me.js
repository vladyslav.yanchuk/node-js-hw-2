const deleteBtn = document.getElementById('delete');
const jwt = localStorage.getItem('token');

const request = async (url, method, data, jwt) => {
  try {
    const options = {
      method,
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': jwt,
      },
    };

    const response = await fetch(url, options);
    const json = await response.json();
    console.log(json);
    return json;
  } catch (error) {
    console.error('Error:', error);
  }
};

window.onload = async (e) => {
  const name = document.getElementById('username__name');
  const created = document.getElementById('created__date');

  const options = {weekday: 'long', year: 'numeric',
    month: 'long', day: 'numeric'};
  const data = await request('http://localhost:8080/api/me', 'GET', undefined, jwt);

  name.innerText = data.user.username;
  created.innerText = new Date(data.user.createdDate)
      .toLocaleDateString('en-US', options);
};

deleteBtn.onclick = (e) => {
  request('http://localhost:8080/api/me', 'DELETE', undefined, jwt);
  localStorage.removeItem('token');
  location.href = './login.html';
};
