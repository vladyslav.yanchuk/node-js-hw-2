const express = require('express');
const jwt = require('jsonwebtoken');
const {authSchema} = require('../validation/validationSchemas');
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const {JWT_SECRET} = require('../config');
const {bcryptPassword} = require('../helpers');

const router = new express.Router();

const returnError = (err, res, code=400, message) => {
  console.log(err);
  res.status(code).json({message});
};

router.post('/register', async (req, res) => {
  try {
    const {username, password} = req.body;
    const {error} = authSchema.validate({username, password});

    if ( error ) {
      returnError('Invalid fields', res, 400, 'Invalid fields');
    }

    const user = new User({
      username,
      password: await bcryptPassword(password),
      createdDate: Date.now(),
    });

    await user.save((err, results) => {
      if ( err ) {
        returnError(err, res, 400, 'Server error');
      } else {
        res.status(200).json({message: 'Successfull registration!'});
      }
    });
  } catch (err) {
    console.log(err);
    returnError(err, res, 400, 'Client error');
  }
});

router.post('/login', async (req, res) => {
  const {username, password} = req.body;
  const {error} = authSchema.validate({username, password});

  if ( error ) {
    returnError('Invalid fields', res, 400, 'Invalid fields');
  } else {
    const user = await User.findOne({
      username: username,
    }).exec();

    if ( !user ) {
      const noUser = `No user with username'${username}' found!`;
      returnError(noUser, res, 400, noUser);
    } else if ( !bcrypt.compareSync(password, user.password) ) {
      returnError('Wrong password!', res, 400, 'Wrong password!');
    } else {
      const token = jwt.sign({username: user.username, id: user.id},
          JWT_SECRET);
      res.status(200).json({message: 'Successful login', jwt_token: token});
    }
  }
});

module.exports = router;
