const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/userModel');
const {authMiddleware} = require('../middleware/authMiddleware');
const Note = require('../models/noteModel');
const {bcryptPassword} = require('../helpers');
const {passSchema} = require('../validation/validationSchemas');

const router = new express.Router();

const returnError = (err, res, code, message) => {
  console.log(err);
  res.status(400).json({message});
};

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {id} = req.data;
    const user = await User.findOne({_id: id},
        {'__v': 0, 'password': 0}).exec();

    res.status(200).json({"user":user});
  } catch ( err ) {
    returnError(err, res, 400, err.message);
  }
});

router.delete('/', authMiddleware, async (req, res) => {
  console.log(req.data);
  try {
    await Note.deleteMany({userId: req.data.id}).exec();
    await User.findOneAndDelete({_id: req.data.id}).exec();
  } catch (err) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }

  res.status(200).json({message: 'Success'});
});

router.patch('/', authMiddleware, async (req, res) => {
  try {
    const {username, id} = req.data;
    const {oldPassword, newPassword} = req.body;

    const user = await User.findOne({
      username,
    }).exec();

    const {error} = passSchema.validate({newPassword});

    if ( !bcrypt.compareSync(oldPassword, user.password) ) {
      returnError('Wrong password!', res, 400, 'Wrong password!');
    } else if (error) {
      returnError('Wrong password!', res, 400, `Unvalid password!`);
    } else {
      const hash = await bcryptPassword(newPassword);
      User.updateOne({_id: id}, {password: hash}, (err) => {
        if ( err ) {
          returnError(err, res, 400, err.message);
        } else {
          res.status(200).json({message: 'Success'});
        }
      });
    }
  } catch (err) {
    returnError(err, res, 400, err.message);
  }
});

module.exports = router;
