const express = require('express');
const Note = require('../models/noteModel');
const {authMiddleware} = require('../middleware/authMiddleware');

const router = new express.Router();

const returnError = (err, res, code, message) => {
  console.log(err);
  res.status(400).json({message});
};

router.post('/', authMiddleware, async (req, res) => {
  const {id} = req.data;
  const note = new Note({
    text: req.body.text,
    userId: id,
    completed: false,
    createDate: Date.now(),
  });

  await note.save((err, results) => {
    if ( err ) {
      returnError(err, res, 400, 'Server error');
    } else {
      res.status(200).json({message: 'Note created!'});
    }
  });
});

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {id} = req.data;
    const userNotes = await Note.find({
      userId: id,
    }, {'__v': 0}).exec();

    res.status(200).json({notes: userNotes});
  } catch ( err ) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }
});

router.get('/:id', authMiddleware, async (req, res) => {
  try {
    const _id = req.params.id;
    const note = await Note.findOne({
      userId: req.data.id,
      _id,
    }, {'__v': 0}).exec();
      res.status(200).json({"note":note});
  } catch ( err ) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }
});

router.put('/:id', authMiddleware, async (req, res) => {
  try {
    const _id = req.params.id;
    const text = req.body.text;
    const filter = {userId: req.data.id, _id};
    const update = {text};
    await Note.findOneAndUpdate(filter, update).exec();

    res.status(200).json({message: 'Success'});
  } catch ( err ) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }
});

router.patch('/:id', authMiddleware, async (req, res) => {
  try {
    const _id = req.params.id;
    const filter = {userId: req.data.id, _id};
    const note = await Note.findOne(filter).exec();
    await Note.updateOne(filter, {completed: !note.completed}).exec();

    res.status(200).json({message: 'Success'});
  } catch ( err ) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }
});

router.delete('/:id', authMiddleware, async (req, res) => {
  try {
    const _id = req.params.id;
    const filter = {userId: req.data.id, _id};
    await Note.findOneAndDelete(filter).exec();

    res.status(200).json({message: 'Success'});
  } catch ( err ) {
    console.log(err);
    returnError(err, res, 400, err.message);
  }
});

module.exports = router;
