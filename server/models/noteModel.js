const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  createDate: {
    type: Date,
    required: true,
  },
});

module.exports = Note;
