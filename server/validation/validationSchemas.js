const Joi = require('joi');

module.exports = {
  authSchema: Joi.object({
    username: Joi.string()
        .required()
        .max(30),

    password: Joi.string()
        .required()
        .min(3)
        .max(30),
  }),
  passSchema: Joi.object({
    newPassword: Joi.string()
        .required()
        .min(3)
        .max(30),
  }),
};
